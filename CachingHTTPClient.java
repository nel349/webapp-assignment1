import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CachingHTTPClient {
	static URL url = null;
	static boolean cached =false;
	static String filepath = "/tmp/nel349/assignment1/";

	static String max_age;
	static String s_maxage;

	static URLConnection connection;

	static String hashedURL;
	static boolean  doCache = false;	
	static String age = 0 + "\n";

	/*
	 * HEURISTIC Conditional Variables
	 */
	static boolean withMaxAge= false;
	static boolean withSmaxAge= false;
	static boolean withExpires = false;
	static boolean withNoCache = false;
	static boolean withLastModified = false;


	public static void main(String []args) throws IOException {
		//Pre-processing.. creating cache directories
		pre_processing();


		if (args.length < 1) {
			System.out.println("Usage:");
			System.out.println("java CachingHTTPClient <host>");
			System.exit(0);
		}

		File f= null;
		try {
			url = new URL(args[0]);
			hashedURL = url.toString().replace('/', '#');
			f = new File(filepath + hashedURL);
			if(f.exists() && !f.isDirectory()){
				cached = true;
			}


		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		assert url != null;

		if(!cached){
			try {

				general();
				System.out.println("***** Serving from the source – start *****");
				System.out.println("Content-Encoding:" + connection.getContentEncoding());
				System.out.println("Content-Length:" + connection.getContentLength());
				System.out.println("Content-Type:" + connection.getContentType());

				System.out.println("Date:" + connection.getHeaderField("Date"));
				System.out.println("Transfer-Encoding:" + connection.getHeaderField("Transfer-Encoding"));
				System.out.println("Transfer-Length:" + connection.getHeaderField("Transfer-Length"));
				System.out.println("Expires:" + connection.getHeaderField("Expires"));
				System.out.println("Cache-Control:" + connection.getHeaderField("Cache-Control"));
				System.out.println("Connection:" + connection.getHeaderField("Connection"));

				if(doCache){
					doCache();	
					doCache=false;
				}
				else{
					InputStream input = connection.getInputStream();
					byte[] buffer = new byte[4096];
					int n = - 1;


					String header = max_age + "\n"+ s_maxage + "\n"+ connection.getHeaderField("Date") + "\n"+ connection.getHeaderField("Cache-Control") + "\n" + connection.getContentEncoding() +"\n" + connection.getContentLength() + "\n" + connection.getContentType() + "\n"
							+ connection.getHeaderField("Transfer-Encoding") + "\n"+ connection.getHeaderField("Transfer-Length") + "\n"+ connection.getHeaderField("Expires") + "\n"+ 
							connection.getHeaderField("Connection") +"\n";



					while ( (n = input.read(buffer)) != -1)
					{
						if (n > 0)
						{
							System.out.write(buffer, 0, n);
						}
					}

				}
				System.out.println("***** Serving from the source – end *****");


			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			try {
				getCache(f);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}

	}


	public static void doCache() throws IOException{
		//		connection = url.openConnection();

		InputStream input = connection.getInputStream();
		byte[] buffer = new byte[4096];
		int n = - 1;
		FileOutputStream f = new FileOutputStream(filepath + hashedURL);


		String header = max_age + "\n"+ s_maxage + "\n"+ connection.getHeaderField("Date") + "\n"+ connection.getHeaderField("Cache-Control") + "\n" + connection.getContentEncoding() +"\n" + connection.getContentLength() + "\n" + connection.getContentType() + "\n"
				+ connection.getHeaderField("Transfer-Encoding") + "\n"+ connection.getHeaderField("Transfer-Length") + "\n"+ connection.getHeaderField("Expires") + "\n"+ 
				connection.getHeaderField("Connection") +"\n";

		byte agebyte[] = age.getBytes();
		byte b[] = header.getBytes();
		f.write(agebyte,0, agebyte.length);
		f.write(b, 0, b.length);

		while ( (n = input.read(buffer)) != -1)
		{
			if (n > 0)
			{
				System.out.write(buffer, 0, n);
				f.write(buffer, 0, n);
			}
		}
		f.close();


	}

	public static void getCache(File file ) throws IOException, ParseException{

		/*
		 * 0: Age
		 * 1: Max-age
		 * 2: s-maxage
		 * 3: Date
		 * 
		 */
		FileInputStream fis = null;

		Scanner sc = new Scanner(file);

		long max_age= 0;
		long s_maxage=0;
		String s_e_date = ""; 


		String a= sc.nextLine();
		String b= sc.nextLine();
		String c= sc.nextLine();

		if(!a.equals("null")&& !c.isEmpty()){
		}
		if(!b.equals("null")&& !c.isEmpty()){
			max_age =Long.parseLong(b);
		}
		if(!c.equals("null") && !c.isEmpty()){
			s_maxage= Long.parseLong(c);
		}
		s_e_date = sc.nextLine();



		sc.close();
		SimpleDateFormat e_date = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
		Date d1 = e_date.parse(s_e_date);
		Date d2 = new Date();

		long delta_age= (d2.getTime() - d1.getTime())/1000;

		//if delta_age < max_age: We can still cache from this website
		//else if delta_age >= max_age: We need to renew the cache.

		if(delta_age < max_age){
			System.out.println("***** Serving from the cache – start *****");
			age = delta_age + "\n";
			try {
				fis = new FileInputStream(file);
				int content;
				while ((content = fis.read()) != -1) {
					// convert to char and display it
					System.out.print((char) content);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fis != null)
						fis.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
			System.out.println("Age: " + delta_age);

			System.out.println("***** Serving from the cache – end *****");

		}else{
			System.out.println("***** Serving from the source – start *****");

			connection = url.openConnection();
			File x = new File(filepath + hashedURL);
			x.delete();
			age = delta_age +"\n";

			general();
			doCache();
			System.out.println("***** Serving from the source – end *****");

		}

	}


	public static void pre_processing() throws IOException{
		Path p1 = Paths.get("/tmp/nel349/assignment1");
		if (Files.notExists(p1)) {
			System.out.println("Cache Directory does not exist");
			File file = new File("/tmp/nel349/assignment1/init.txt");
			file.getParentFile().mkdirs();

		}
	}

	public static void general() throws IOException{
		connection = url.openConnection();


		String cache_control = connection.getHeaderField("Cache-Control");
		String expires = connection.getHeaderField("Expires");
		String last_modified = connection.getHeaderField("Last-Modified");


		if(cache_control == null ){// or cache_control = no-cache, no-store, must-revalidate
			System.out.println("No Cache-Control");
			//Proceed to some heuristic

		}else{

			//Max-Age
			String regexMax_Age = ".*max-age=(\\d+).*";
			Pattern pattern = Pattern.compile(regexMax_Age);
			Matcher max_age_matcher;
			max_age_matcher= pattern.matcher(cache_control);	
			if(max_age_matcher.find()){
				max_age = max_age_matcher.group(1);
				int m= Integer.parseInt(max_age);
				if(m > 0){
					withMaxAge = true;
				}
			}

			//No-cache detection
			String regexCache = ".*(no-cache|no-store|must-revalidate).*";
			Pattern patternCache = Pattern.compile(regexCache);
			Matcher no_cache =  patternCache.matcher(cache_control);

			if(no_cache.find() && !withMaxAge){
				withNoCache = true;
			}





			//S-maxage				
			String regexS_Age = ".*s-maxage=(\\d+).*";
			Pattern patternS_Age = Pattern.compile(regexS_Age);
			Matcher s_maxage_matcher;
			s_maxage_matcher= patternS_Age.matcher(cache_control);	
			if(s_maxage_matcher.find() && !withMaxAge){
				s_maxage = s_maxage_matcher.group(1);
				int m= Integer.parseInt(s_maxage);
				if(m > 0){
					withSmaxAge = true;
				}
			}else{
				s_maxage = "null";
			}


		}


		//Expires
		if(expires != null && !expires.isEmpty() && !expires.equals("-1") && !expires.equals("0")){ // Need to add wehen expires is -1
			SimpleDateFormat e_date = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
			Date d1= null;
			try {
				d1 = e_date.parse(expires);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Date d2 = new Date();
			long delta_age= (d2.getTime() - d1.getTime())/1000;

			if(delta_age <= 0){
				withExpires = true;
			}

		}

		long delta_age_modified =0;
		if(last_modified != null && !last_modified.isEmpty() && !last_modified.equals("-1") && !last_modified.equals("0")){ // Need to add wehen expires is -1
			SimpleDateFormat e_date = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
			Date d1= null;
			try {
				d1 = e_date.parse(last_modified);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Date d2 = new Date();
			delta_age_modified = (d1.getTime() - d2.getTime())/10000;

			withLastModified = true;

		}


		/*
		 * HEURISTIC SWITCH
		 */
		if(withMaxAge){
			doCache = true;
		}
		else if(withSmaxAge){
			doCache = true;
		}
		else if(withExpires){
			doCache = true;	
		}
		else if(withNoCache){
			doCache = false;
		}
		else if(withLastModified){
			max_age = delta_age_modified + "\n";
			doCache = true;
		}


	}


}
