Assignment1 Caching HTTP Client

Norman E. Lopez
UTEID: nel349

Heuristics:

»How do you detect a resource is available in your cache?
	The way I give my resources a unique identifier is by replacing all of the '/' characters by '#'. I rename this identifier hashedURL which is the name of my resource. Then if my hashedURL exists in my cache directory we do not create a new resource.

»How do you handle dynamic content?
	"You don't have to worry about dynamic content." - DevDatta 

HEURISTICS:

1. Firstly, want to check if our cache-control header contains the following directives: max-age and s-maxage

	if max-age is contained, we cache our file and then perform the regular protocol.
	else if max-ege is not contained we search for s-maxage
		if s-maxage is found we treat it such as max-age in this assignment.

	if no max-age or s-maxage directives are found, we then consider the "expires" header. 
		By determining the difference between current time and the expires date, we can set a time for our cache to be stored.

	if neither previous headers and directives  existed, then we consider other directives inside our cache-control header such as "no-store", "no-cache", or "must-revalidate". Finding these directives will determine that we don't have to cache our file.

	Finally, if nothing of the previous headers and directives were found, we use our "Last Modified " header. Subtracting our current time from the modified header and multipy by 0.10. Suggested by the rfc2616 13.2.4(Expiration Calculations). This is done considering that if the page has been modified recently, most likely it will be modified later in time(soon). 


	sources: rfc2616 13.2.4
	code: Stackoverflow.com : This website was helpful to obtain examples of how to subtract Dates and how to {open, delete, read} files and directories.  

