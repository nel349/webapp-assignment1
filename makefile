all:

clean:
	rm -f  *.gcda
	rm -f  *.gcno
	rm -f  *.gcov
	rm -f  CachingHTTPClient.class


compile:
	javac CachingHTTPClient.java

run: compile
	java CachingHTTPClient http://www.google.com
	java CachingHTTPClient http://www.youtube.com
	java CachingHTTPClient http://www.nbc.com/
	java CachingHTTPClient http://www.facebook.com
	java CachingHTTPClient http://www.amazon.com
	java CachingHTTPClient http://www.microsoft.com
	java CachingHTTPClient http://www.excelsior.com.mx